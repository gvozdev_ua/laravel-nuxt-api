<p align="center"><img src="https://laravel.com/assets/img/components/logo-laravel.svg"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Развёртывание проэкта

Для развёрки проэкта

Установить зависимости
```
composer install
```

При необходимости, выдать права пользователю на 
storage и bootstrap/cache

Добавить .env из .env.example

Cгенерировать ключ

```
php artisan key:generate
```

Создать и подключить доступы в БД в .env

Запустить миграции и сиды

```
php artisan migrate:fresh --seed
```

Запутить сервер

```
php artisan serv --port:3001
```

###Важно! Бек и фронт должны быть на одном домене.
